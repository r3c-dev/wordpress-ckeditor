<?php
use R3C\Wordpress\EventDispatcher\EventDispatcher;

EventDispatcher::addListener(function() {
    add_action( 'wp_enqueue_scripts', 'r3cWordpressCkEditorScripts' );
});

function r3cWordpressCkEditorScripts() {
    $url = str_replace('/wp', '/', get_site_url());
    $js = $url . 'wp-content/plugins/wordpress-ckeditor/js/';

	if ( current_user_can('manage_options')) {
		wp_enqueue_script('ckeditor', $js . 'ckeditor/ckeditor.js', array(), '1.0');
	}
}

?>